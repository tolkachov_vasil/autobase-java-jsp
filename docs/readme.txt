### **FinalWebProject** ###

**Разработать веб-систему согласно требованиям.**

Общие требования к проекту:

* Приложение реализовать применяя технологии Servlet и JSP.

* Архитектура приложения должна соответствовать шаблонам Layered architecture и Model-View-Controller.

* Информация о предметной области должна хранится в БД:

    1. данные в базе хранятся на кириллице, рекомендуется применять кодировку utf-8
    1. технология доступа к БД - JDBC (только JDBC)
    1. для работы с БД в приложении должен быть реализован пул соединений
    1. при проектировании БД рекомендуется не использовать более 6-8 таблиц
    1. доступ к данным в приложении осуществлять с использованием шаблона DAO.
    1. Интерфейс приложения должен быть локализован; выбор языков: английский, русский.

* Приложение должно корректно обрабатывать возникающие исключительные ситуации, в том числе вести их логи.

* В качестве логгера использовать Log4J или SLF-J.

* Классы и другие сущности приложения должны быть грамотно структурированы по пакетам и иметь отражающую их функциональность название.

* При реализации бизнес-логики приложения следует при необходимости использовать шаблоны проектирования (например, шаблоны GoF: Factory Method, Command, Builder, Strategy, State, Observer. Singleton etc)

* также необходимо избегать процедурного стиля программирования.

* Для хранения пользовательской информации между запросами использовать сессию.

* Для перехвата и корректировки объектов 3anpoca(request) и ответа(response) применить фильтры.

* При реализации страниц JSP следует использовать теги библиотеки JSTL.
* использовать скриплеты запрещено.

* Обязательным требованием является реализация и использование пользовательских тегов.

* Просмотр "длинных списков" желательно организовывать в постраничном режиме.

* Документацию к проекту необходимо оформить согласно требованиям javadoc.

* Оформление кода должно соответствовать Java Code Convention.

* Разрешается использовать технологию Maven.

* Приложение должно содержать 3-4 теста JUnit.

* Общие требования к функциональности проекта:

    1. Bxoд(sign in) и Bыход(sign out) в из системы.
    1. Регистрация пользователя.
    1. Просмотр информации (например: просмотр всех курсов, имеющихся кредитных карт, счетов и т.д.)
    1. Удаление информации (например: отмена заказа, медицинского назначения, отказ от курса обучения и т.д.)
    1. Добавление и модификация информации (например: создать и отредактировать курс, создать и отредактировать заказ и т.д.)

### Система Автобаза ###
 **Диспетчер** распределяет **Заявки** на **Рейсы** между **Водителями**,
за каждым из которых закреплен свой **Автомобиль**.
На **Рейс** может быть назначен **Автомобиль**,
находящийся в исправном состоянии и характеристики которого
 соответствуют **Заявке**.
 **Водитель** делает отметку о выполнении **Рейса** и состоянии **Автомобиля**.