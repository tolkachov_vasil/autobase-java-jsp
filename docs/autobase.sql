CREATE DATABASE IF NOT EXISTS `autobase` /*!40100 DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci */;
USE `autobase`;
-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: autobase
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS car;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE car (
  id          INT(11)                 NOT NULL AUTO_INCREMENT,
  brand       VARCHAR(20)             NOT NULL,
  model       VARCHAR(40)             NOT NULL,
  `number`    VARCHAR(10)             NOT NULL,
  capacity_kg INT(11)                 NOT NULL,
  driver      INT(11)                          DEFAULT NULL,
  `status`    ENUM('valid', 'broken') NOT NULL DEFAULT 'valid',
  PRIMARY KEY (id),
  UNIQUE KEY `number` (`number`),
  KEY driver (driver),
  CONSTRAINT car_ibfk_1 FOREIGN KEY (driver) REFERENCES `user` (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

INSERT INTO car VALUES (1, 'ГАЗ', 'Газель', '1822 МН-7', 1700, 1, 'valid');
INSERT INTO car VALUES (2, 'Volkswagen ', 'Сrafter', '6651 МН-5', 3500, 2, 'valid');
INSERT INTO car VALUES (3, 'Mersedes', 'Sprinter', '7475 МН-7', 3190, 3, 'valid');
INSERT INTO car VALUES (4, 'Ford', 'Transit', '8825 МН-7', 4100, 4, 'broken');
INSERT INTO car VALUES (5, 'Peugeot', 'Boxer', '4712 МН-4', 2198, 5, 'broken');

--
-- Table structure for table `tender`
--

DROP TABLE IF EXISTS tender;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE tender (
  id     INT(11)     NOT NULL AUTO_INCREMENT,
  driver INT(11)              DEFAULT NULL,
  weight INT(11)     NOT NULL,
  city   VARCHAR(40) NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tender`
--

INSERT INTO tender VALUES (1, 1, 1500, 'Гродно');
INSERT INTO tender VALUES (2, 3, 3600, 'Krakov');
INSERT INTO tender VALUES (3, NULL, 3000, 'Москва');
INSERT INTO tender VALUES (4, NULL, 2100, 'Полоцк');
INSERT INTO tender VALUES (5, NULL, 4000, 'Брест');
INSERT INTO tender VALUES (6, NULL, 1250, 'Киев');
INSERT INTO tender VALUES (7, NULL, 1750, 'Вильнюс');

--
-- Table structure for table `trip`
--

DROP TABLE IF EXISTS trip;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE trip (
  id       INT(11)                           NOT NULL AUTO_INCREMENT,
  car      INT(11)                                    DEFAULT NULL,
  tender   INT(11)                                    DEFAULT NULL,
  `status` ENUM('idle', 'accepted', 'ended') NOT NULL DEFAULT 'idle',
  PRIMARY KEY (id),
  KEY car (car),
  KEY tender (tender),
  CONSTRAINT trip_ibfk_1 FOREIGN KEY (car) REFERENCES car (id),
  CONSTRAINT trip_ibfk_2 FOREIGN KEY (tender) REFERENCES tender (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip`
--
INSERT INTO trip VALUES (1, 1, 1, 'idle');
INSERT INTO trip VALUES (2, 2, 2, 'idle');
--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS user;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  id         INT(11)                                        NOT NULL AUTO_INCREMENT,
  `name`     VARCHAR(40)                                    NOT NULL,
  surname    VARCHAR(40)                                    NOT NULL,
  login      VARCHAR(20)                                    NOT NULL,
  `password` VARCHAR(20)                                    NOT NULL,
  role       ENUM('guest', 'driver', 'dispatcher', 'admin') NOT NULL DEFAULT 'guest',
  PRIMARY KEY (id),
  UNIQUE KEY login (login)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

INSERT INTO user VALUES (1, 'Николай', 'Иванов', 'iii', '111', 'driver');
INSERT INTO user VALUES (2, 'Александр', 'Петров', 'petr', '111', 'driver');
INSERT INTO user VALUES (3, 'Анна', 'Кравцова', 'anny', '111', 'driver');
INSERT INTO user VALUES (4, 'John', 'Smith', 'jsss', '111', 'driver');
INSERT INTO user VALUES (5, 'Татьяна', 'Кузнецова', 'kuza', '111', 'driver');
INSERT INTO user VALUES (6, 'Ольга', 'Плотникова', 'ppp', '111', 'dispatcher');
INSERT INTO user VALUES (7, 'Иван', 'Грозный', 'admin', '111', 'admin');

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed
