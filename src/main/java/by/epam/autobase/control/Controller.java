package by.epam.autobase.control;

import by.epam.autobase.control.command.factory.ActionFactory;
import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.resource.Constant;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.ConfigurationManager;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@WebServlet(
        name = "autobase",
        urlPatterns = "/controller",
        loadOnStartup = 1,
        initParams = @WebInitParam(
                name = "log4j-properties-location",
                value = "log4j.properties"))
public class Controller extends HttpServlet {

    private static final long serialVersionUID = 3193513864734871439L;

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = null;
        ActionFactory actionFactory = new ActionFactory();
        IActionCommand command = actionFactory.defineCommand(request);
        try {
            page = command.execute(request);
        } catch (ServiceException e) {
            e.printStackTrace();
            request.getSession().setAttribute("ex", e.getMessage());
            page = ConfigurationManager.getProperty(Constant.PATH_PAGE_ERROR);
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String log4jPropLocation = getInitParameter("log4j-properties-location");
        String webAppPath = getServletContext().getRealPath(File.separator);
        PropertyConfigurator.configure(webAppPath + log4jPropLocation);
    }

}
