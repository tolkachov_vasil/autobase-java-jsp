package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.model.dao.UserDao;
import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.subject.User;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;

public class AdminEditUserChoiceCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        try {

            User user = new UserDao().findById(
                    Long.parseLong(
                            request.getParameter(
                                    Constant.PARAM_SELECTED_ID)));

            request.setAttribute(Constant.PARAM_USER, user);
            request.setAttribute(Constant.PARAM_TABLE_COMMAND_SELECTOR, Constant.PARAM_EDIT_ENTITY);
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return Constant.CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_USER;
    }
}
