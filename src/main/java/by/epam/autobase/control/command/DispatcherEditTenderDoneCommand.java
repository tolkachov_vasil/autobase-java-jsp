package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;

public class DispatcherEditTenderDoneCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        long driver = Long.parseLong(request.getParameter("driver"));
        long id = Long.parseLong(request.getParameter("tenderId"));
        Service.updateTenderDriver(id, driver);
        return Constant.CONTROLLER_COMMAND_DISPATCHER$SHOW_TENDERS;
    }
}
