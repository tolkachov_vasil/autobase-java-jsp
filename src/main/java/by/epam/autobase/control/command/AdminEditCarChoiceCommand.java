package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.resource.Constant;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.model.dao.CarDao;
import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.subject.Car;

import javax.servlet.http.HttpServletRequest;

public class AdminEditCarChoiceCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {

            Car car = new CarDao().findById(
                    Long.parseLong(request.getParameter(
                            Constant.PARAM_SELECTED_ID)));

            request.setAttribute(Constant.PARAM_CAR, car);
            request.setAttribute(Constant.PARAM_TABLE_COMMAND_SELECTOR, Constant.PARAM_EDIT_ENTITY);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return Constant.CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_CAR;
    }
}
