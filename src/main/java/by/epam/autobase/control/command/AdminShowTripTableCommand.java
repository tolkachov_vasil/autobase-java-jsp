package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.ConfigurationManager;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AdminShowTripTableCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request)
            throws ServiceException {

        HttpSession session = request.getSession();

        session.setAttribute(Constant.PARAM_CONTENT_SELECTOR, Constant.PARAM_SHOW_TABLE);

        session.setAttribute(Constant.PARAM_SELECT_TABLE, Constant.PARAM_TRIP);

        session.setAttribute(Constant.PARAM_RESULT_TABLE, Service.takeAllTrips());

        return ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
    }
}
