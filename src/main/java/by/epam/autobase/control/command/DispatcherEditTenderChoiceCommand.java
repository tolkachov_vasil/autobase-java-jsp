package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.model.dao.TenderDao;
import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.subject.Tender;
import by.epam.autobase.model.subject.User;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class DispatcherEditTenderChoiceCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        try {
            Tender tender = new TenderDao().findById(Long.parseLong(request.getParameter(Constant.PARAM_SELECTED_ID)));
            request.setAttribute(Constant.PARAM_TENDER, tender);
            request.setAttribute(Constant.PARAM_TABLE_COMMAND_SELECTOR, Constant.PARAM_EDIT_ENTITY);
            ArrayList<User> drivers = Service.takeValidDriversForTender(tender);
            request.setAttribute(Constant.PARAM_VALID_DRIVERS, drivers);
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return Constant.CONTROLLER_COMMAND_DISPATCHER$SHOW_TENDERS;
    }
}
