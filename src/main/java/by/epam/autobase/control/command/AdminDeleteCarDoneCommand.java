package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;

public class AdminDeleteCarDoneCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request)
            throws ServiceException {

        String sid = request.getParameter(Constant.PARAM_SELECTED_ID);
        long id = Long.parseLong(sid);
        Service.deleteCar(id);

        return Constant.CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_CAR;
    }
}
