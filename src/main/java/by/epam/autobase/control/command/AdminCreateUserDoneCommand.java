package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author Vasil Talkachou
 */
public class AdminCreateUserDoneCommand implements IActionCommand {

    public AdminCreateUserDoneCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        HashMap<String, String[]> map = new HashMap<>(request.getParameterMap());
        Service.createUser(map);
        return (Constant.CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_USER);
    }

}
