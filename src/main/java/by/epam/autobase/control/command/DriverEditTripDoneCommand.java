package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.model.subject.Trip;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;

public class DriverEditTripDoneCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        Trip trip = (Trip) request.getSession().getAttribute(Constant.PARAM_TRIP);
        Service.updateTrip(trip, request.getParameter(Constant.PARAM_STATUS));

        return Constant.CONTROLLER_COMMAND_DRIVER$SHOW_TRIP;
    }
}
