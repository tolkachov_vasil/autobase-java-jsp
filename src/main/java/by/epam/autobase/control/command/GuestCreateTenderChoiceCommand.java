package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.ConfigurationManager;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;

public class GuestCreateTenderChoiceCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        request.setAttribute(Constant.PARAM_CONTENT_SELECTOR, Constant.PARAM_SHOW_ADD_TENDER_FORM);

        return ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
    }
}
