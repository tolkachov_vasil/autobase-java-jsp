package by.epam.autobase.control.command.factory;

import by.epam.autobase.control.command.*;

/**
 * involves enumeration for parameters (kinds of commands) creates an object of
 * necessary Command Class
 */
public enum CommandEnum {

    //    CMD {{this.command = new Command();}},

    CREATE_TENDER_DONE {{
        this.command = new GuestCreateTenderDoneCommand();
    }},

    CREATE_TENDER {{
        this.command = new GuestCreateTenderChoiceCommand();
    }},

    DRIVER$EDIT_CAR_DONE {{
        this.command = new DriverEditCarDoneCommand();
    }},

    DRIVER$EDIT_TRIP_DONE {{
        this.command = new DriverEditTripDoneCommand();
    }},

    DRIVER$SHOW_TRIP {{
        this.command = new DriverShowTripChoiceCommand();
    }},
    DRIVER$SHOW_CAR {{
        this.command = new DriverShowCarChoiceCommand();
    }},
    ADMIN$DELETE_TRIP_DONE {{
        this.command = new AdminDeleteTripDoneCommand();
    }},
    ADMIN$EDIT_TRIP_DONE {{
        this.command = new AdminEditTripDoneCommand();
    }},
    ADMIN$EDIT_TRIP_SELECT {{
        this.command = new AdminEditTripChoiceCommand();
    }},
    ADMIN$CREATE_TRIP_DONE {{
        this.command = new AdminCreateTripDoneCommand();
    }},

    ADMIN$DELETE_TENDER_DONE {{
        this.command = new AdminDeleteTenderDoneCommand();
    }},
    DISPATCHER$DELETE_TENDER_DONE {{
        this.command = new DispatcherDeleteTenderDoneCommand();
    }},
    ADMIN$EDIT_TENDER_DONE {{
        this.command = new AdminEditTenderDoneCommand();
    }},
    DISPATCHER$EDIT_TENDER_DONE {{
        this.command = new DispatcherEditTenderDoneCommand();
    }},
    ADMIN$EDIT_TENDER_SELECT {{
        this.command = new AdminEditTenderChoiceCommand();
    }},
    DISPATCHER$EDIT_TENDER_SELECT {{
        this.command = new DispatcherEditTenderChoiceCommand();
    }},
    ADMIN$CREATE_TENDER_DONE {{
        this.command = new AdminCreateTenderDoneCommand();
    }},

    ADMIN$DELETE_CAR_DONE {{
        this.command = new AdminDeleteCarDoneCommand();
    }},
    ADMIN$EDIT_CAR_DONE {{
        this.command = new AdminEditCarDoneCommand();
    }},
    ADMIN$EDIT_CAR_SELECT {{
        this.command = new AdminEditCarChoiceCommand();
    }},
    ADMIN$CREATE_CAR_DONE {{
        this.command = new AdminCreateCarDoneCommand();
    }},

    ADMIN$DELETE_USER_DONE {{
        this.command = new AdminDeleteUserDoneCommand();
    }},
    ADMIN$EDIT_USER_DONE {{
        this.command = new AdminEditUserDoneCommand();
    }},
    ADMIN$EDIT_USER_SELECT {{
        this.command = new AdminEditUserChoiceCommand();
    }},
    ADMIN$CREATE_USER_DONE {{
        this.command = new AdminCreateUserDoneCommand();
    }},

    ADMIN$CREATE_ENTITY_SELECT {{
        this.command = new AdminCreateEntityChoiceCommand();
    }},
    SHOW_LOGIN_FORM {{
        this.command = new ShowLoginFormCommand();
    }},
    ADMIN$SHOW_TABLE_TENDER {{
        this.command = new AdminShowTenderTableCommand();
    }},
    DISPATCHER$SHOW_TENDERS {{
        this.command = new DispatcherShowTendersCommand();
    }},
    DISPATCHER$SHOW_DRIVERS {{
        this.command = new DispatcherShowDriversCommand();
    }},
    ADMIN$SHOW_TABLE_USER {{
        this.command = new AdminShowUserTableCommand();
    }},
    ADMIN$SHOW_TABLE_CAR {{
        this.command = new AdminShowCarTableCommand();
    }},
    ADMIN$SHOW_TABLE_TRIP {{
        this.command = new AdminShowTripTableCommand();
    }},
    DISPATCHER$SHOW_TRIPS {{
        this.command = new DispatcherShowTripsCommand();
    }},
    LOGIN {{
        this.command = new LoginCommand();
    }},
    SET_RU_LOCALE {{
        this.command = new SetRuLocaleCommand();
    }},
    SET_EN_LOCALE {{
        this.command = new SetEnLocaleCommand();
    }},
    LOGOUT {{
        this.command = new LogoutCommand();
    }};

    IActionCommand command;

    public IActionCommand getCommand() {
        return command;
    }
}
