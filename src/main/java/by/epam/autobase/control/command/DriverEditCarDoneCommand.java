package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.model.subject.Car;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;

public class DriverEditCarDoneCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        Car car = (Car) request.getSession().getAttribute(Constant.PARAM_CAR);
        Service.updateCar(car, request.getParameter(Constant.PARAM_STATUS));

        return Constant.CONTROLLER_COMMAND_DRIVER$SHOW_CAR;
    }
}
