package by.epam.autobase.control.command.factory;

import by.epam.autobase.resource.Constant;
import by.epam.autobase.resource.ContentManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ActionFactory {

    private static final Logger LOGGER = LogManager.getLogger(ActionFactory.class);

    public IActionCommand defineCommand(HttpServletRequest request) {
        HttpSession session = request.getSession();
        IActionCommand command = new EmptyCommand();
        String actionName = request.getParameter(Constant.PARAM_COMMAND);
        if (actionName == null || actionName.isEmpty()) {
            LOGGER.info("command not detected, use: " + command.getClass());
            return command;
        }
        try {
            CommandEnum commandEnum = CommandEnum.valueOf(actionName.toUpperCase());
            command = commandEnum.getCommand();
        } catch (IllegalArgumentException e) {
            LOGGER.fatal("wrong command detected: " + actionName.toUpperCase());
            request.setAttribute(Constant.PARAM_WRONG_ACTION, actionName
                    + ContentManager.getProperty(
                    (String) session.getAttribute(Constant.PARAM_LOCALE),
                    Constant.MESSAGE_WRONGACTION));
        }
        LOGGER.info("command detected: " + command);

        return command;
    }
}