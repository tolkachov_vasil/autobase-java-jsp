package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.resource.ConfigurationManager;
import by.epam.autobase.resource.Constant;
import by.epam.autobase.resource.ContentManager;

import javax.servlet.http.HttpServletRequest;

public class LogoutCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        String locale = (String) request.getSession().getAttribute(Constant.PARAM_LOCALE);
        String message = ContentManager.getProperty(locale, Constant.MESSAGE_YOU_LOGOUT);
        request.setAttribute(Constant.PARAM_MESSAGE, message);
        // destroy the session
        request.getSession().invalidate();
        return ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
    }
}
