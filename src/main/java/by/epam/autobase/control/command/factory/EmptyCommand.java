package by.epam.autobase.control.command.factory;

import by.epam.autobase.resource.Constant;
import by.epam.autobase.resource.ConfigurationManager;
import javax.servlet.http.HttpServletRequest;

public class EmptyCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        return ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
    }
}
