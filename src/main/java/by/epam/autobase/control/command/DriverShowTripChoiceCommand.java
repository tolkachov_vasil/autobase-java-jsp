package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.resource.Constant;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.model.subject.User;
import by.epam.autobase.resource.ConfigurationManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DriverShowTripChoiceCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();

        session.setAttribute(
            Constant.PARAM_CONTENT_SELECTOR,
                Constant.PARAM_SHOW_TRIP);

        User user = (User) session.getAttribute(Constant.PARAM_THIS_USER);

        session.setAttribute(
                Constant.PARAM_TRIP,
            Service.takeTripByDriverId(user.getId()));

        session.setAttribute(
                Constant.PARAM_TENDER,
            Service.takeTenderByDriverId(user.getId()));

        return ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
    }
}
