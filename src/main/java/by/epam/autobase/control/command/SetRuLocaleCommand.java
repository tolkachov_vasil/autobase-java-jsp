package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.resource.Constant;
import by.epam.autobase.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class SetRuLocaleCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(Constant.PARAM_LOCALE, Constant.LOCALE_RU);
        return ConfigurationManager.getProperty(Constant.PATH_PAGE_INDEX);
    }
}
