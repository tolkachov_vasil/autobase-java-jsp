package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.ConfigurationManager;
import by.epam.autobase.resource.Constant;
import by.epam.autobase.resource.ContentManager;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public class GuestCreateTenderDoneCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        HashMap<String, String[]> map = new HashMap<>(request.getParameterMap());
        Service.createTender(map);

        String locale = (String) request.getSession().getAttribute(Constant.PARAM_LOCALE);
        String message = ContentManager.getProperty(locale, Constant.MESSAGE_TENDER_CREATED);
        request.setAttribute(Constant.PARAM_MESSAGE, message);

        return ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
    }
}
