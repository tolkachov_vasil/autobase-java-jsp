package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public class AdminEditCarDoneCommand implements IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        HashMap<String, String[]> map = new HashMap<>(request.getParameterMap());
        Service.updateCar(map);
        return (Constant.CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_CAR);
    }
}
