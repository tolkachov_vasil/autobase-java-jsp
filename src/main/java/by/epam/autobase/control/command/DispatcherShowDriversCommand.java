package by.epam.autobase.control.command;

import by.epam.autobase.resource.Constant;
import by.epam.autobase.logic.Service;
import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DispatcherShowDriversCommand implements by.epam.autobase.control.command.factory.IActionCommand {

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();

        session.setAttribute(
                Constant.PARAM_CONTENT_SELECTOR,
                Constant.PARAM_SHOW_TABLE);

        session.setAttribute(
                Constant.PARAM_SELECT_TABLE,
                Constant.PARAM_USER);

        session.setAttribute(
                Constant.PARAM_ALL_DRIVERS,
                Service.takeAllDrivers());

        session.setAttribute(
                Constant.PARAM_ALL_CARS,
                Service.takeAllCars());

        return ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
    }
}
