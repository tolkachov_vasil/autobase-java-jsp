package by.epam.autobase.control.command.factory;

import by.epam.autobase.logic.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

public interface IActionCommand {

    String execute(HttpServletRequest request) throws ServiceException;
}
