package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.logic.Service;
import by.epam.autobase.model.subject.User;
import by.epam.autobase.resource.ConfigurationManager;
import by.epam.autobase.resource.Constant;
import by.epam.autobase.resource.ContentManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginCommand implements IActionCommand {

    private static final Logger LOGGER = LogManager.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String page;
        String locale = (String) session.getAttribute(Constant.PARAM_LOCALE);
        String login = request.getParameter(Constant.PARAM_LOGIN);
        String pass = request.getParameter(Constant.PARAM_PASSWORD);
        User user = Service.takeUserByLoginAndPassword(login, pass);
        if (user != null) {
            // set user role for web page content management
            session.setAttribute(Constant.PARAM_ROLE, user.getRole());
            // clear content selector
            session.setAttribute(Constant.PARAM_CONTENT_SELECTOR, "");
            session.setAttribute(Constant.PARAM_THIS_USER, user);
            LOGGER.info("Login ok");
            String message = ContentManager.getProperty(locale, Constant.MESSAGE_YOU_SIGNED_AS);
            String role = ContentManager.getProperty(locale, "role." + user.getRole());
            request.setAttribute(Constant.PARAM_MESSAGE, message + " " + role);
            page = ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
        } else {
            String message = ContentManager.getProperty(locale, Constant.MESSAGE_LOGIN_ERROR);
            request.setAttribute(Constant.PARAM_MESSAGE, message);
            LOGGER.info("Login Error");
            page = ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
        }
        return page;
    }
}
