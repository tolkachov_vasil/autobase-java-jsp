package by.epam.autobase.control.command;

import by.epam.autobase.control.command.factory.IActionCommand;
import by.epam.autobase.resource.ConfigurationManager;
import by.epam.autobase.resource.Constant;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Vasil Talkachou
 */
public class AdminCreateEntityChoiceCommand implements IActionCommand {

    public AdminCreateEntityChoiceCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute(Constant.PARAM_TABLE_COMMAND_SELECTOR, Constant.PARAM_CREATE_ENTITY);
        return ConfigurationManager.getProperty(Constant.PATH_PAGE_MAIN);
    }

}
