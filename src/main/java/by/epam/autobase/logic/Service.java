package by.epam.autobase.logic;

import by.epam.autobase.logic.exception.ServiceException;
import by.epam.autobase.model.dao.CarDao;
import by.epam.autobase.model.dao.TenderDao;
import by.epam.autobase.model.dao.TripDao;
import by.epam.autobase.model.dao.UserDao;
import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.subject.Car;
import by.epam.autobase.model.subject.Tender;
import by.epam.autobase.model.subject.Trip;
import by.epam.autobase.model.subject.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;

public class Service {

    private static final Logger LOGGER
            = LogManager.getLogger(Service.class);

    private static Service instance = new Service();

    private Service() {
    }

    public static User takeUserByLoginAndPassword(String login, String password) {
        User user = new UserDao().findUserByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public static ArrayList<User> takeAllUsers()
            throws ServiceException {
        ArrayList<User> list;
        try {
            list = new UserDao().findAll();
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return list;
    }

    public static ArrayList<Car> takeAllCars()
            throws ServiceException {
        ArrayList<Car> list;
        try {
            list = new CarDao().findAll();
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return list;
    }

    public static ArrayList<Tender> takeAllTenders()
            throws ServiceException {
        ArrayList<Tender> list;
        try {
            list = new TenderDao().findAll();
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return list;
    }

    public static ArrayList<Trip> takeAllTrips()
            throws ServiceException {
        ArrayList<Trip> list;
        try {
            list = new TripDao().findAll();
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return list;
    }

    public static void createUser(HashMap<String, String[]> map)
            throws ServiceException {

        User user = new User();
        user.setName(map.get("name")[0]);
        user.setSurname(map.get("surname")[0]);
        user.setLogin(map.get("login")[0]);
        user.setPassword(map.get("password")[0]);
        user.setRole(map.get("role")[0]);
        try {
            new UserDao().create(user);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void createCar(HashMap<String, String[]> map)
            throws ServiceException {
        Car car = new Car();
        car.setDriver(Long.parseLong(map.get("driver")[0]));
        car.setBrand(map.get("brand")[0]);
        car.setModel(map.get("model")[0]);
        car.setNumber(map.get("number")[0]);
        car.setCapacity(Integer.parseInt(map.get("capacity")[0]));
        car.setStatus(map.get("status")[0]);
        try {
            new CarDao().create(car);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void createTrip(long tender, long driver)
            throws ServiceException {
        Car car = takeCarByDriverId(driver);
        Trip trip = new Trip();
        trip.setCar(car.getId());
        trip.setTender(tender);
        trip.setStatus("idle");
        try {
            new TripDao().create(trip);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void createTrip(HashMap<String, String[]> map)
            throws ServiceException {
        Trip trip = new Trip();
        trip.setCar(Long.parseLong(map.get("car")[0]));
        trip.setTender(Long.parseLong(map.get("tender")[0]));
        trip.setStatus(map.get("status")[0]);
        try {
            new TripDao().create(trip);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void createTender(HashMap<String, String[]> map)
            throws ServiceException {
        Tender tender = new Tender();
        tender.setDriver(Long.parseLong(map.get("driver")[0]));
        tender.setWeight(Integer.parseInt(map.get("weight")[0]));
        tender.setCity(map.get("city")[0]);
        try {
            new TenderDao().create(tender);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void updateUser(HashMap<String, String[]> map)
            throws ServiceException {
        User user = new User();
        user.setId(Long.parseLong(map.get("id")[0]));
        user.setName(map.get("name")[0]);
        user.setSurname(map.get("surname")[0]);
        user.setLogin(map.get("login")[0]);
        user.setPassword(map.get("password")[0]);
        user.setRole(map.get("role")[0]);
        try {
            new UserDao().update(user);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void updateCar(HashMap<String, String[]> map)
            throws ServiceException {
        Car car = new Car();
        car.setId(Long.parseLong(map.get("id")[0]));
        car.setDriver(Long.parseLong(map.get("driver")[0]));
        car.setBrand(map.get("brand")[0]);
        car.setModel(map.get("model")[0]);
        car.setNumber(map.get("number")[0]);
        car.setCapacity(Integer.parseInt(map.get("capacity")[0]));
        car.setStatus(map.get("status")[0]);
        try {
            new CarDao().update(car);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void updateTrip(HashMap<String, String[]> map)
            throws ServiceException {
        Trip trip = new Trip();
        trip.setId(Long.parseLong(map.get("id")[0]));
        trip.setCar(Long.parseLong(map.get("car")[0]));
        trip.setTender(Long.parseLong(map.get("tender")[0]));
        trip.setStatus(map.get("status")[0]);
        try {
            new TripDao().update(trip);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void updateTender(HashMap<String, String[]> map)
            throws ServiceException {
        Tender tender = new Tender();
        tender.setId(Long.parseLong(map.get("id")[0]));
        tender.setDriver(Long.parseLong(map.get("driver")[0]));
        tender.setWeight(Integer.parseInt(map.get("weight")[0]));
        tender.setCity(map.get("city")[0]);
        try {
            new TenderDao().update(tender);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void deleteUser(long id)
            throws ServiceException {
        try {
            new UserDao().delete(id);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void deleteCar(long id)
            throws ServiceException {
        try {
            new CarDao().delete(id);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void deleteTender(long id)
            throws ServiceException {
        try {
            new TenderDao().delete(id);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void deleteTrip(long id)
            throws ServiceException {
        try {
            new TripDao().delete(id);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static ArrayList<User> takeAllDrivers() throws ServiceException {
        ArrayList<User> list;
        try {
            list = new UserDao().findAllDrivers();
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return list;
    }

    public static void updateTenderDriver(long tender, long driver)
            throws ServiceException {
        try {
            new TenderDao().updateDriverId(tender, driver);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        if (driver != 0) {
            createTrip(tender, driver);
        } else {
            deleteTripByTenderId(tender);
        }
    }

    public static void deleteTripByTenderId(long tender)
            throws ServiceException {

        try {
            new TripDao().deleteByTenderId(tender);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static ArrayList<User> takeValidDriversForTender(Tender tender)
            throws ServiceException {
        int weight = tender.getWeight();
        ArrayList<User> drivers = null;
        try {
            drivers = new UserDao().findVacantDriversForCarCapacity(weight);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return drivers;
    }

    public static Car takeCarByDriverId(long id)
            throws ServiceException {

        Car car = null;
        try {
            car = new CarDao().findByUserId(id);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return car;
    }

    public static Trip takeTripByDriverId(long id)
            throws ServiceException {

        Trip trip = null;
        try {
            trip = new TripDao().findByUserId(id);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return trip;
    }

    public static Tender takeTenderByDriverId(long id)
            throws ServiceException {

        Tender tender = null;
        try {
            tender = new TenderDao().findByUserId(id);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
        return tender;
    }

    public static void updateCar(Car car, String status)
            throws ServiceException {

        car.setStatus(status);
        try {
            new CarDao().update(car);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }

    public static void updateTrip(Trip trip, String status)
            throws ServiceException {

        trip.setStatus(status);
        try {
            new TripDao().update(trip);
        } catch (DAOException e) {
            LOGGER.fatal(e.getMessage());
            throw new ServiceException(e);
        }
    }
}
