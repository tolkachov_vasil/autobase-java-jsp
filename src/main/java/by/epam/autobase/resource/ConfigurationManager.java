package by.epam.autobase.resource;

import java.io.Serializable;
import java.util.ResourceBundle;

public class ConfigurationManager implements Serializable {

    private final static ResourceBundle resourceBundle
            = ResourceBundle.getBundle(Constant.FILE_CONFIG_PROP);

    private ConfigurationManager() {
    }

    public static String getProperty(String key) {
        String value = null;
        if (resourceBundle.containsKey(key)) {
            value = resourceBundle.getString(key);
        }
        return value;
    }
}
