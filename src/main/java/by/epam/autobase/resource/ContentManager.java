package by.epam.autobase.resource;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

// класс извлекает информацию из файла pagecontent.properties
public class ContentManager {

    private static final Map<String, ResourceBundle> RESOURCE_BUNDLE = new HashMap<>();

    static {
        RESOURCE_BUNDLE.put(Constant.LOCALE_RU, ResourceBundle.getBundle(
                Constant.FILE_PAGE_CONTENT_PROP, Locale.forLanguageTag(Constant.LOCALE_RU)));
        RESOURCE_BUNDLE.put(Constant.LOCALE_EN, ResourceBundle.getBundle(
                Constant.FILE_PAGE_CONTENT_PROP, Locale.forLanguageTag(Constant.LOCALE_EN)));
    }

    private ContentManager() {
    }

    public static String getProperty(String locale, String key) {
        return RESOURCE_BUNDLE.get(locale).getString(key);
    }

}
