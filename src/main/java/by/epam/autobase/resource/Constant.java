package by.epam.autobase.resource;

public class Constant {

    public static final String CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_CAR = "/controller?command=admin$show_table_car";
    public static final String CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_TENDER = "/controller?command=admin$show_table_tender";
    public static final String CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_TRIP = "/controller?command=admin$show_table_trip";
    public static final String CONTROLLER_COMMAND_ADMIN$SHOW_TABLE_USER = "/controller?command=admin$show_table_user";
    public static final String CONTROLLER_COMMAND_DISPATCHER$SHOW_TENDERS = "/controller?command=dispatcher$show_tenders";
    public static final String CONTROLLER_COMMAND_DRIVER$SHOW_CAR = "/controller?command=driver$show_car";
    public static final String CONTROLLER_COMMAND_DRIVER$SHOW_TRIP = "/controller?command=driver$show_trip";
    public static final String FILE_CONFIG_PROP = "config";
    public static final String FILE_PAGE_CONTENT_PROP = "pagecontent";
    public static final String LOCALE_EN = "en";
    public static final String LOCALE_RU = "ru";
    public static final String MESSAGE_LOGIN_ERROR = "message.login_error";
    public static final String MESSAGE_TENDER_CREATED = "message.tender_created";
    public static final String MESSAGE_WRONGACTION = "message.wrongaction";
    public static final String MESSAGE_YOU_LOGOUT = "message.you_logout";
    public static final String MESSAGE_YOU_SIGNED_AS = "message.you_signed_as";
    public static final String PARAM_ALL_CARS = "allCars";
    public static final String PARAM_ALL_DRIVERS = "allDrivers";
    public static final String PARAM_ALL_TENDERS = "allTenders";
    public static final String PARAM_ALL_TRIPS = "allTrips";
    public static final String PARAM_CAR = "car";
    public static final String PARAM_COMMAND = "command";
    public static final String PARAM_CONTENT_SELECTOR = "selector";
    public static final String PARAM_CREATE_ENTITY = "createEntity";
    public static final String PARAM_EDIT_ENTITY = "editEntity";
    public static final String PARAM_LOCALE = "locale";
    public static final String PARAM_LOGIN = "login";
    public static final String PARAM_MESSAGE = "message";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_RESULT_TABLE = "resultTable";
    public static final String PARAM_ROLE = "role";
    public static final String PARAM_SELECT_TABLE = "selectTable";
    public static final String PARAM_SELECTED_ID = "selectedid";
    public static final String PARAM_SHOW_ADD_TENDER_FORM = "showAddTenderForm";
    public static final String PARAM_SHOW_CAR = "showCar";
    public static final String PARAM_SHOW_LOGIN_FORM = "showLoginForm";
    public static final String PARAM_SHOW_TABLE = "showTable";
    public static final String PARAM_SHOW_TRIP = "showTrip";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_TABLE_COMMAND_SELECTOR = "tableCommandSelector";
    public static final String PARAM_TENDER = "tender";
    public static final String PARAM_THIS_USER = "thisUser";
    public static final String PARAM_TRIP = "trip";
    public static final String PARAM_USER = "user";
    public static final String PARAM_VALID_DRIVERS = "validDrivers";
    public static final String PARAM_WRONG_ACTION = "wrongAction";
    public static final String PATH_PAGE_ERROR = "path.page.error";
    public static final String PATH_PAGE_INDEX = "path.page.index";
    public static final String PATH_PAGE_MAIN = "path.page.main";
    public static final String USER_ROLE_ADMIN = "admin";
    public static final String USER_ROLE_DISPATCHER = "dispatcher";
    public static final String USER_ROLE_DRIVER = "driver";
    public static final String USER_ROLE_GUEST = "guest";
}

