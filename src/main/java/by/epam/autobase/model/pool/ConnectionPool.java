package by.epam.autobase.model.pool;

import by.epam.autobase.model.pool.exception.ConnectionPoolException;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class ConnectionPool.
 */
public class ConnectionPool {

    /**
     * Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    /**
     * Constant POOL_SIZE.
     */
    private static final int POOL_SIZE = 5;
    /**
     * Constant URL.
     */
    private static final String URL = "url";
    /**
     * Constant LOGIN.
     */
    private static final String LOGIN = "login";
    /**
     * Constant PASSWORD.
     */
    private static final String PASSWORD = "password";
    /**
     * instance.
     */
    private static ConnectionPool instance;
    /**
     * lock.
     */
    private static Lock lock = new ReentrantLock();
    /**
     * take pool atomic.
     */
    private static AtomicBoolean takeConnectionAtomic = new AtomicBoolean(true);
    /**
     * create pool atomic.
     */
    private static AtomicBoolean createPoolAtomic = new AtomicBoolean(false);
    /**
     * pool.
     */
    private BlockingQueue<ProxyConnection> pool;

    /**
     * Instantiates a new pool pool.
     *
     * @throws ConnectionPoolException pool pool exception
     */
    private ConnectionPool() throws ConnectionPoolException {
        init();
    }

    /**
     * Represents double-checked singleton pattern that gets  only instance
     * of {@code ConnectionPool}.
     *
     * @return single instance of {@code ConnectionPool}.
     */
    public static ConnectionPool getInstance() {

        if (!createPoolAtomic.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    createPoolAtomic.set(true);
                }
            } catch (ConnectionPoolException e) {
                LOGGER.log(Level.WARN, "Error while Connection Pool initialization. Trying init again...");

            } finally {

                lock.unlock();
            }
        }

        return instance;
    }

    /**
     * Inits .
     *
     * @throws ConnectionPoolException pool pool exception
     */
    private void init() throws ConnectionPoolException {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            Properties property = new Properties();
            InputStream input = new FileInputStream(
                    this.getClass().getClassLoader().getResource("database.properties").getFile());
            property.load(input);

            String url = property.getProperty(URL);
            String login = property.getProperty(LOGIN);
            String password = property.getProperty(PASSWORD);

            pool = new ArrayBlockingQueue<>(POOL_SIZE);
            for (int i = 0; i <= POOL_SIZE; i++) {
                ProxyConnection connection = new ProxyConnection(DriverManager.getConnection(url, login, password));
                pool.offer(connection);
            }
        } catch (SQLException | IOException | ClassNotFoundException e) {

            throw new ConnectionPoolException(e);

        }

    }

    /**
     * Gets an instance of {@code ConnectionImpl} from  pool using non-
     * blocking {@code poll()} method.
     *
     * @return an instance of {@code ConnectionImpl}.
     */
    public ProxyConnection takeConnection() {

        ProxyConnection connection = null;
        if (takeConnectionAtomic.get()) {
            try {
                connection = pool.take();

            } catch (InterruptedException e) {
                LOGGER.error("InterruptedException in ConnectionPool class, takeConnection()", e);
            }
        }
        return connection;
    }

    /**
     * Gives back a non-null pool to  pool pool through  use of
     * {@code add()} method.
     *
     * @param connection is an instance of {@code ConnectionImpl}.
     */
    public void returnConnection(ProxyConnection connection) {
        try {
            if (!connection.isClosed()) {
                pool.offer(connection);
            }
        } catch (SQLException e) {
            LOGGER.error("SQLException in ConnectionPool class, returnConnection(Connection pool)", e);
        }
    }

    /**
     * Shuts down  pool pool by closing all  connections in it. Uses
     * {@code sleep} method for compulsory delay before cleaning  pool.
     */
    public void cleanUp() {
        takeConnectionAtomic = new AtomicBoolean(false);
        final int WAITING_RETURN_THREADS_TO_POOL = 50;

        try {
            TimeUnit.MILLISECONDS.sleep(WAITING_RETURN_THREADS_TO_POOL);

            Iterator<ProxyConnection> iterator = pool.iterator();
            while (iterator.hasNext()) {
                ProxyConnection connection = iterator.next();
                if (connection != null) {
                    connection.destruct();
                }
                iterator.remove();
            }
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException in ConnectionPool class, cleanUp()", e);
        } catch (SQLException e) {
            LOGGER.error("SQLException in ConnectionPool class, cleanUp method", e);
        }
    }
}
