package by.epam.autobase.model.pool;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class AppListener implements ServletContextListener {
	private static final Logger LOGGER = LogManager.getLogger(AppListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		int counter = 3;
		ConnectionPool connectionPool;
		do {
			connectionPool = ConnectionPool.getInstance();
			if (connectionPool != null) {
				LOGGER.log(Level.INFO, "Connection pool has been initialized.");
			}

		} while (connectionPool == null && --counter != 0);

		if (connectionPool == null) {
			LOGGER.log(Level.FATAL, "Error in initializing the connection pool!");
			throw new RuntimeException("Fatal Error! Connection pool can't be initialized!");
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ConnectionPool connectionPool = ConnectionPool.getInstance();

		if (connectionPool != null) {
			ConnectionPool.getInstance().cleanUp();
		}
	}
}