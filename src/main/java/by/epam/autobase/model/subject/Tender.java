package by.epam.autobase.model.subject;

public class Tender extends Entity implements IEntity {

    private long id;
    private long driver;
    private int weight;
    private String city;

    public Tender() {
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getDriver() {
        return driver;
    }

    public void setDriver(long driver) {
        this.driver = driver;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Tender{" +
                "id=" + id +
                ", driver=" + driver +
                ", weight=" + weight +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public String toRawString() {
        return id + "," + driver + "," + weight + "," + city;
    }

}
