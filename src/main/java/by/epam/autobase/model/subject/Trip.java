package by.epam.autobase.model.subject;

public class Trip extends Entity implements IEntity {

    private long id;
    private long car;
    private long tender;
    private String status;

    public Trip() {
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getCar() {
        return car;
    }

    public void setCar(long car) {
        this.car = car;
    }

    public long getTender() {
        return tender;
    }

    public void setTender(long tender) {
        this.tender = tender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", car=" + car +
                ", tender=" + tender +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public String toRawString() {
        return id + "," + car + "," + tender + ",'" + status;
    }
}
