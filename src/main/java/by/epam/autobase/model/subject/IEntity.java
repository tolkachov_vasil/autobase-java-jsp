package by.epam.autobase.model.subject;

public interface IEntity {

    long getId();

    void setId(long id);

    String toRawString();
}
