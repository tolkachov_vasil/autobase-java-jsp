package by.epam.autobase.model.subject;

public class Car extends Entity implements IEntity {

    private long id;
    private long driver;
    private String brand;
    private String model;
    private String number;
    private int capacity;
    private String status;

    public Car() {
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getDriver() {
        return driver;
    }

    public void setDriver(long driver) {
        this.driver = driver;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", driver=" + driver +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", number='" + number + '\'' +
                ", capacity=" + capacity +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public String toRawString() {
        return id +
                "," + driver +
                "," + brand +
                "," + model +
                "," + number +
                "," + capacity +
                "," + status;
    }
}
