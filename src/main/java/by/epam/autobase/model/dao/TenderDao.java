package by.epam.autobase.model.dao;

import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.pool.ProxyConnection;
import by.epam.autobase.model.subject.Tender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TenderDao extends AbstractDao implements IDao<Tender> {

    private static final String SQL_SELECT_ALL_TENDERS
            = "SELECT * FROM autobase.tender";

    private final static String SQL_SELECT_TENDER_BY_ID
            = "SELECT * FROM autobase.tender WHERE id=?";

    private final static String SQL_SELECT_TENDER_BY_USER_ID
            = "SELECT tender.*\n" +
            "FROM autobase.tender\n" +
            "  LEFT JOIN autobase.user\n" +
            "    ON user.id = tender.driver\n" +
            "WHERE user.id = ?";

    private final static String SQL_DELETE_TENDER_BY_ID
            = "DELETE FROM autobase.tender WHERE tender.id=?";

    private static final String SQL_CREATE_TENDER
            = "INSERT INTO autobase.tender (driver, weight, city) VALUES (?, ?, ?)";

    private static final String SQL_UPDATE_TENDER
            = "UPDATE autobase.tender SET driver=?, weight=?, city=? WHERE id=?";

    private static final String SQL_UPDATE_TENDER_DRIVER_ID
            = "UPDATE autobase.tender SET driver=? WHERE id=?";

    private static final Logger LOGGER = LogManager.getLogger(TenderDao.class);

    public TenderDao(ProxyConnection connection) {
        super(connection);
    }

    public TenderDao() {
    }

    private Tender takeTenderFromResutSet(ResultSet resultSet) throws SQLException {
        Tender tender = new Tender();
        tender.setId(resultSet.getLong("id"));
        tender.setDriver(resultSet.getLong("driver"));
        tender.setWeight(resultSet.getInt("weight"));
        tender.setCity(resultSet.getString("city"));
        LOGGER.debug("parsed tender:" + tender);
        return tender;
    }

    @Override
    public ArrayList findAll() throws DAOException {
        ArrayList<Tender> tenders = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_ALL_TENDERS);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Tender tender = takeTenderFromResutSet(resultSet);
                tenders.add(tender);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return tenders;
    }

    @Override
    public Tender findById(long id) throws DAOException {
        Tender tender = new Tender();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_TENDER_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                tender = takeTenderFromResutSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return tender;
    }

    public Tender findByUserId(long id) throws DAOException {
        Tender tender = new Tender();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_TENDER_BY_USER_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                tender = takeTenderFromResutSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return tender;
    }

    @Override
    public boolean delete(long id) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_DELETE_TENDER_BY_ID);
            st.setLong(1, id);
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            close(st);
        }
        return true;
    }

    @Override
    public boolean delete(Tender entity) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Tender entity) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_CREATE_TENDER);
            st.setLong(1, entity.getDriver());
            st.setInt(2, entity.getWeight());
            st.setString(3, entity.getCity());
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return true;
    }

    @Override
    public Tender update(Tender entity) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_UPDATE_TENDER);
            st.setLong(1, entity.getDriver());
            st.setInt(2, entity.getWeight());
            st.setString(3, entity.getCity());
            st.setLong(4, entity.getId());
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return entity;
    }

    public void updateDriverId(long id, long driver) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_UPDATE_TENDER_DRIVER_ID);
            st.setLong(1, driver);
            st.setLong(2, id);
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
    }
}
