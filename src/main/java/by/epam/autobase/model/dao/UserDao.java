package by.epam.autobase.model.dao;

import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.pool.ProxyConnection;
import by.epam.autobase.model.subject.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDao extends AbstractDao implements IDao<User> {

    private static final String SQL_SELECT_ALL_USERS
            = "SELECT * FROM autobase.user";

    private final static String SQL_SELECT_USER_BY_ID
            = "SELECT * FROM autobase.user WHERE id=?";

    private final static String SQL_SELECT_USER_BY_LOGIN
            = "SELECT * FROM autobase.user WHERE login=?";

    private final static String SQL_SELECT_ALL_DRIVERS
            = "SELECT * FROM autobase.user WHERE role = 'driver' ";

    private final static String SQL_SELECT_ALL_VACANT_DRIVERS
            = "SELECT\n" +
            "  user.id,\n" +
            "  user.name,\n" +
            "  user.surname\n" +
            "FROM autobase.user\n" +
            "  LEFT JOIN autobase.tender\n" +
            "    ON user.id = tender.driver\n" +
            "  LEFT JOIN autobase.car\n" +
            "    ON user.id = car.driver\n" +
            "  LEFT JOIN autobase.trip\n" +
            "    ON car.id = trip.car\n" +
            "WHERE user.role = 'driver'\n" +
            "      AND car.capacity_kg > ?\n" +
            "      AND tender.driver IS NULL\n" +
            "      AND trip.status IS NULL";

    private final static String SQL_DELETE_USER_BY_ID
            = "DELETE FROM autobase.user WHERE user.id=?";

    private static final String SQL_CREATE_USER
            = "INSERT INTO autobase.user (name, surname, login, password, role) VALUES (?, ?, ?, ?, ?)";

    private static final String SQL_UPDATE_USER
            = "UPDATE autobase.user SET name=?, surname=?, login=?, password=?, role=? WHERE id=?";

    private static final Logger LOGGER = LogManager.getLogger(UserDao.class);

    public UserDao(ProxyConnection connection) {
        super(connection);
    }

    public UserDao() {
    }

    private User takeUserFromResutSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong("id"));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        user.setRole(resultSet.getString("role").toLowerCase());
        LOGGER.debug("parsed user:" + user);
        return user;
    }

    @Override
    public User findById(long id) throws DAOException {
        User user = new User();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_USER_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                user = takeUserFromResutSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return user;
    }

    @Override
    public ArrayList<User> findAll() throws DAOException {
        ArrayList<User> users = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_ALL_USERS);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                User user = takeUserFromResutSet(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return users;
    }

    @Override
    public boolean delete(long id) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_DELETE_USER_BY_ID);
            st.setLong(1, id);
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            close(st);
        }
        return true;
    }

    @Override
    public boolean delete(User entity) throws DAOException {
        return false;
    }

    @Override
    public boolean create(User entity) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_CREATE_USER);
            st.setString(1, entity.getName());
            st.setString(2, entity.getSurname());
            st.setString(3, entity.getLogin());
            st.setString(4, entity.getPassword());
            st.setString(5, entity.getRole().toUpperCase());
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return true;
    }

    @Override
    public User update(User entity) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_UPDATE_USER);
            st.setString(1, entity.getName());
            st.setString(2, entity.getSurname());
            st.setString(3, entity.getLogin());
            st.setString(4, entity.getPassword());
            st.setString(5, entity.getRole().toUpperCase());
            st.setLong(6, entity.getId());
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return entity;
    }

    public User findUserByLogin(String login) {
        User user = null;
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_USER_BY_LOGIN);
            st.setString(1, login);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                user = takeUserFromResutSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            close(st);
        }
        return user;
    }

    public ArrayList<User> findAllDrivers()
            throws DAOException {
        ArrayList<User> users = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_ALL_DRIVERS);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong("id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                users.add(user);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return users;
    }

    public ArrayList<User> findVacantDriversForCarCapacity(int weight)
            throws DAOException {

        ArrayList<User> users = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_ALL_VACANT_DRIVERS);
            st.setInt(1, weight);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong("id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                users.add(user);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return users;
    }

}
