package by.epam.autobase.model.dao;

import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.pool.ProxyConnection;
import by.epam.autobase.model.subject.Trip;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TripDao extends AbstractDao implements IDao<Trip> {

    private static final String SQL_SELECT_ALL_TRIPS
        = "SELECT * FROM autobase.trip";

    private final static String SQL_SELECT_TRIP_BY_USER_ID
        = "SELECT trip.*\n"
        + "FROM autobase.user\n"
        + "  LEFT JOIN autobase.car\n"
        + "    ON user.id = car.driver\n"
        + "  LEFT JOIN autobase.trip\n"
        + "    ON car.id = trip.car\n"
        + "WHERE user.id = ?";

    private final static String SQL_SELECT_TRIP_BY_ID
        = "SELECT * FROM autobase.trip WHERE id=?";

    private final static String SQL_DELETE_TRIP_BY_ID
        = "DELETE FROM autobase.trip WHERE trip.id=?";

    private final static String SQL_DELETE_TRIP_BY_TENDER_ID
        = "DELETE FROM autobase.trip WHERE trip.tender=?";

    private static final String SQL_CREATE_TRIP
        = "INSERT INTO autobase.trip (car, tender, status) VALUES (?,?,?)";

    private static final String SQL_UPDATE_TRIP
        = "UPDATE autobase.trip SET car=?, tender=?, status=? WHERE id=?";

    private static final Logger LOGGER = LogManager.getLogger(TripDao.class);

    public TripDao(ProxyConnection connection) {
        super(connection);
    }

    public TripDao() {
    }

    private Trip takeTripFromResutSet(ResultSet resultSet) throws SQLException {
        Trip trip = new Trip();
        trip.setId(resultSet.getLong("id"));
        trip.setCar(resultSet.getLong("car"));
        trip.setTender(resultSet.getLong("tender"));
        trip.setStatus(resultSet.getString("status"));
        LOGGER.debug("parsed trip:" + trip);
        return trip;
    }

    @Override
    public ArrayList<Trip> findAll() throws DAOException {
        ArrayList<Trip> trips = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_ALL_TRIPS);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Trip trip = takeTripFromResutSet(resultSet);
                trips.add(trip);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return trips;
    }

    @Override
    public Trip findById(long id) throws DAOException {
        Trip trip = new Trip();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_TRIP_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                trip = takeTripFromResutSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return trip;
    }

    @Override
    public boolean delete(long id) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_DELETE_TRIP_BY_ID);
            st.setLong(1, id);
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            close(st);
        }
        return true;
    }

    @Override
    public boolean delete(Trip entity) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Trip entity) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_CREATE_TRIP);
            st.setLong(1, entity.getCar());
            st.setLong(2, entity.getTender());
            st.setString(3, entity.getStatus().toUpperCase());
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return true;
    }

    @Override
    public Trip update(Trip entity) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_UPDATE_TRIP);
            st.setLong(1, entity.getCar());
            st.setLong(2, entity.getTender());
            st.setString(3, entity.getStatus().toUpperCase());
            st.setLong(4, entity.getId());
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return entity;
    }

    public Trip findByUserId(long id) throws DAOException {
        Trip trip = new Trip();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_TRIP_BY_USER_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                trip = takeTripFromResutSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return trip;
    }

    public void deleteByTenderId(long tender)
        throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_DELETE_TRIP_BY_TENDER_ID);
            st.setLong(1, tender);
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            close(st);
        }
    }
}
