package by.epam.autobase.model.dao;

import by.epam.autobase.model.pool.ConnectionPool;
import by.epam.autobase.model.pool.ProxyConnection;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractDao {

    private static final Logger LOGGER = LogManager.getLogger(AbstractDao.class);
    protected ProxyConnection connection;

    public AbstractDao(ProxyConnection connection) {
        this.connection = connection;
    }

    public AbstractDao() {
        this.connection = ConnectionPool.getInstance().takeConnection();
    }

    public void close(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Error with closing statement");
        }
        ConnectionPool.getInstance().returnConnection(connection);
    }
}
