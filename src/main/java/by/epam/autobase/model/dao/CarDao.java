package by.epam.autobase.model.dao;

import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.pool.ProxyConnection;
import by.epam.autobase.model.subject.Car;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CarDao extends AbstractDao implements IDao<Car> {

    private static final String SQL_SELECT_ALL_CARS
            = "SELECT * FROM autobase.car";

    private final static String SQL_SELECT_CAR_BY_ID
            = "SELECT * FROM autobase.car WHERE id=?";

    private final static String SQL_SELECT_CAR_BY_USER_ID
            = "SELECT * FROM autobase.car WHERE driver=?";

    private final static String SQL_DELETE_CAR_BY_ID
            = "DELETE FROM autobase.car WHERE car.id=?";

    private static final String SQL_CREATE_CAR
            = "INSERT INTO autobase.car (driver, brand, model, number, capacity_kg, status) VALUES (?,?,?,?,?,?)";

    private static final String SQL_UPDATE_CAR
            = "UPDATE autobase.car SET driver=?, brand=?, model=?, number=?, capacity_kg=?, status=? WHERE id=?";

    private static final Logger LOGGER = LogManager.getLogger(CarDao.class);

    public CarDao(ProxyConnection connection) {
        super(connection);
    }

    public CarDao() {
    }

    private Car takeCarFromResutSet(ResultSet resultSet) throws SQLException {
        Car car = new Car();
        car.setId(resultSet.getLong("id"));
        car.setDriver(resultSet.getLong("driver"));
        car.setBrand(resultSet.getString("brand"));
        car.setModel(resultSet.getString("model"));
        car.setNumber(resultSet.getString("number"));
        car.setCapacity(resultSet.getInt("capacity_kg"));
        car.setStatus(resultSet.getString("status"));
        LOGGER.debug("parsed car:" + car);
        return car;
    }

    @Override
    public ArrayList<Car> findAll() throws DAOException {
        ArrayList<Car> cars = new ArrayList<>();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_ALL_CARS);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Car car = takeCarFromResutSet(resultSet);
                cars.add(car);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return cars;
    }

    @Override
    public Car findById(long id) throws DAOException {
        Car car = new Car();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_CAR_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                car = takeCarFromResutSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return car;
    }

    @Override
    public boolean delete(long id) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_DELETE_CAR_BY_ID);
            st.setLong(1, id);
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            close(st);
        }
        return true;
    }

    @Override
    public boolean delete(Car entity) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Car entity) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_CREATE_CAR);
            st.setLong(1, entity.getDriver());
            st.setString(2, entity.getBrand());
            st.setString(3, entity.getModel());
            st.setString(4, entity.getNumber());
            st.setInt(5, entity.getCapacity());
            st.setString(6, entity.getStatus().toUpperCase());
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return true;
    }

    @Override
    public Car update(Car entity) throws DAOException {
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_UPDATE_CAR);
            st.setLong(1, entity.getDriver());
            st.setString(2, entity.getBrand());
            st.setString(3, entity.getModel());
            st.setString(4, entity.getNumber());
            st.setInt(5, entity.getCapacity());
            st.setString(6, entity.getStatus().toUpperCase());
            st.setLong(7, entity.getId());
            st.execute();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return entity;
    }

    public Car findByUserId(long id) throws DAOException {
        Car car = new Car();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement(SQL_SELECT_CAR_BY_USER_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                car = takeCarFromResutSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DAOException(e);
        } finally {
            close(st);
        }
        return car;
    }

}