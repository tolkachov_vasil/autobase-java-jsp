package by.epam.autobase.model.dao;

import by.epam.autobase.model.dao.exception.DAOException;
import by.epam.autobase.model.subject.Entity;

import java.util.List;

public interface IDao<T extends Entity> {

    List<T> findAll() throws DAOException;

    T findById(long id) throws DAOException;

    boolean delete(long id) throws DAOException;

    boolean delete(T entity) throws DAOException;

    boolean create(T entity) throws DAOException;

    T update(T entity) throws DAOException;

}
