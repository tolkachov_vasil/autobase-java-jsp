package by.epam.autobase.web.filter;

import by.epam.autobase.resource.Constant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "SecurityFilter")
public class SecurityFilter implements Filter {

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        String command = request.getParameter(Constant.PARAM_COMMAND);
        if (!command.contains("$")) {
            chain.doFilter(req, resp);
            return;
        }
        String accessMode = command.split("\\$")[0];
        HttpSession session = request.getSession();
        String role = (String) session.getAttribute(Constant.PARAM_ROLE);
        if (role == null) {
            role = Constant.USER_ROLE_GUEST;
        }
        boolean accessGranted = false;
        switch (accessMode) {
            case "admin":
                accessGranted = (role.equals(Constant.USER_ROLE_ADMIN));
                break;
            case "dispatcher":
                accessGranted = (role.equals(Constant.USER_ROLE_DISPATCHER));
                break;
            case "driver":
                accessGranted = (role.equals(Constant.USER_ROLE_DRIVER));
                break;
        }
        if (accessGranted) {
            chain.doFilter(req, resp);
        } else {

            request.setAttribute(Constant.PARAM_MESSAGE, "You has not permissions for this command!");

            RequestDispatcher dispatcher
                    = request.getServletContext().getRequestDispatcher(
                    "/controller?command=show_Login_Form");

            dispatcher.forward(request, response);
        }
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

}
