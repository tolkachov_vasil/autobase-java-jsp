package by.epam.autobase.web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(filterName = "EncodingFilter")
public class EncodingFilter implements Filter {

    private static final String UTF_8 = "UTF-8";

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        String encoding = req.getCharacterEncoding();
        if (encoding == null || !encoding.equalsIgnoreCase(UTF_8)) {
            req.setCharacterEncoding(UTF_8);
        }
        encoding = resp.getCharacterEncoding();
        if (encoding == null || !encoding.equalsIgnoreCase(UTF_8)) {
            resp.setCharacterEncoding(UTF_8);
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {

    }

}
