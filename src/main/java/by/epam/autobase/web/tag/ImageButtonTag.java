package by.epam.autobase.web.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

@SuppressWarnings("serial")
public class ImageButtonTag extends TagSupport {

    private String url;
    private String img;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setImg(String img) {
        this.img = img;
    }

    /**
     * draw the image button.
     * If URL == "submit", make submit button in the form
     *
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        try {
            if ("submit".equalsIgnoreCase(url)) {
                JspWriter out = pageContext.getOut();
                out.print("<input type=\"image\" src=\"/img/");
                out.print(img.toLowerCase());
                out.print(".png\">");
            } else {
                JspWriter out = pageContext.getOut();
                out.print("<a href = \"");
                out.print(url.toLowerCase());
                out.print("\" >");
                out.print("<img class=\"button-img\" src = \"/img/");
                out.print(img.toLowerCase());
                out.print(".png\"></a>");
            }
        } catch (IOException ioe) {
            throw new JspException("Error: " + ioe.getMessage());
        }
        return SKIP_BODY;
    }

}

