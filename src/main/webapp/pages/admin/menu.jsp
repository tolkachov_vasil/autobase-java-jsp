<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<li><a href="/controller?command=admin$show_table_user">
    <fmt:message key="menu.admin.show_users" bundle="${rb}"/>
</a></li>

<li><a href="/controller?command=admin$show_table_car">
    <fmt:message key="menu.admin.show_cars" bundle="${rb}"/>
</a></li>

<li><a href="/controller?command=admin$show_table_trip">
    <fmt:message key="menu.admin.show_trips" bundle="${rb}"/>
</a></li>

<li><a href="/controller?command=admin$show_table_tender">
    <fmt:message key="menu.admin.show_tenders" bundle="${rb}"/>
</a></li>
