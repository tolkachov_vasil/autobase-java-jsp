<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="trip" class="by.epam.autobase.model.subject.Trip"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=admin$edit_trip_done">
    <input type="hidden" name="return" value="/controller?command=admin$show_table_trip">
    <input type="hidden" name="id" value="${requestScope.trip.id}"/>
    <table>
        <tr>
            <th><fmt:message key="table.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.tender.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.trip.status" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>${requestScope.trip.id}</td>
            <td>
                <input type="number" title="car id" name="car" value="${requestScope.trip.car}"/>
            </td>
            <td>
                <input type="number" title="tender id" name="tender" value="${requestScope.trip.tender}"/>
            </td>
            <td>
                <select name="status">
                    <option value="idle"
                            <c:if test="${requestScope.trip.status eq 'idle'}">
                                selected
                            </c:if>
                    ><fmt:message key="trip.status.idle" bundle="${rb}"/>
                    </option>
                    <option value="accepted"
                            <c:if test="${requestScope.trip.status eq 'accepted'}">
                                selected
                            </c:if>
                    ><fmt:message key="trip.status.accepted" bundle="${rb}"/>
                    </option>
                    <option value="ended"
                            <c:if test="${requestScope.trip.status eq 'ended'}">
                                selected
                            </c:if>
                    ><fmt:message key="trip.status.ended" bundle="${rb}"/>
                    </option>
                </select>
            </td>
            <td>
                <ct:button img="done" url="submit"/>
            </td>
            <td>
                <ct:button img="cancel" url="/controller?command=admin$show_table&selectTable=trip"/>
            </td>
        </tr>
    </table>
</form>