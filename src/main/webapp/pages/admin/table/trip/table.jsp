<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="trip" class="by.epam.autobase.model.subject.Trip"/>


<table class="nth">
    <tr>
        <th><fmt:message key="table.id" bundle="${rb}"/></th>
        <th><fmt:message key="table.car.id" bundle="${rb}"/></th>
        <th><fmt:message key="table.tender.id" bundle="${rb}"/></th>
        <th><fmt:message key="table.trip.status" bundle="${rb}"/></th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach items="${sessionScope.resultTable}" varStatus="trip">
        <tr>
            <td> ${trip.current.id} </td>
            <td> ${trip.current.car} </td>
            <td> ${trip.current.tender} </td>
            <td><fmt:message key="trip.status.${trip.current.status}" bundle="${rb}"/></td>
            <td>
                <ct:button img="edit" url="/controller?command=admin$edit_trip_select&selectedid=${trip.current.id}"/>
            </td>
            <td>
                <ct:button img="trash" url="/controller?command=admin$delete_trip_done&selectedid=${trip.current.id}"/>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<c:choose>
    <c:when test="${requestScope.tableCommandSelector eq 'createEntity'}">
        <c:import url="/pages/admin/table/trip/create.jsp"/>
    </c:when>

    <c:when test="${requestScope.tableCommandSelector eq 'editEntity'}">
        <c:import url="/pages/admin/table/trip/edit.jsp"/>
    </c:when>

    <c:otherwise>
        <ct:button img="add" url="/controller?command=admin$create_entity_select"/>
    </c:otherwise>
</c:choose>




