<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="trip" class="by.epam.autobase.model.subject.Trip"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=admin$create_trip_done">
    <input type="hidden" name="return" value="/controller?command=admin$show_table_trip">
    <table>
        <tr>
            <th><fmt:message key="table.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.tender.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.trip.status" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>+</td>
            <td>
                <input type="number" title="car id" name="car" value="0"/>
            </td>
            <td>
                <input type="number" title="tender id" name="tender" value="0"/>
            </td>
            <td>
                <select name="status">
                    <option value="idle">idle</option>
                    <option value="accepted">accepted</option>
                    <option value="ended">ended</option>
                </select>
            </td>
            <td>
                <ct:button img="done" url="submit"/>
            </td>
            <td>
                <ct:button img="cancel" url="/controller?command=admin$show_table&selectTable=trip"/>
            </td>
        </tr>
    </table>
</form>