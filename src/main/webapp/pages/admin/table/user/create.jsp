<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="user" class="by.epam.autobase.model.subject.User"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=admin$create_user_done">
    <table>
        <tr>
            <th><fmt:message key="table.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.user.name" bundle="${rb}"/></th>
            <th><fmt:message key="table.user.surname" bundle="${rb}"/></th>
            <th><fmt:message key="form.login.login" bundle="${rb}"/></th>
            <th><fmt:message key="form.login.password" bundle="${rb}"/></th>
            <th><fmt:message key="table.user.role" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <td>+</td>
        <td>
            <input type="text" name="name"/>
        </td>
        <td>
            <input type="text" name="surname"/>
        </td>
        <td>
            <input type="text" name="login"/>
        </td>
        <td>
            <input type="text" name="password"/>
        </td>
        <td>
            <select name="role" size="1">
                <option selected="selected" value="driver"><fmt:message key="role.driver" bundle="${rb}"/></option>
                <option value="dispatcher"><fmt:message key="role.dispatcher" bundle="${rb}"/></option>
                <option value="admin"><fmt:message key="role.admin" bundle="${rb}"/></option>
            </select>
        </td>
        <td>
            <ct:button img="done" url="submit"/>
        </td>
        <td>
            <ct:button img="cancel" url="/controller?command=admin$show_table&selectTable=user"/>
        </td>
    </table>
</form>