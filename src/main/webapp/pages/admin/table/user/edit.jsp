<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="user" class="by.epam.autobase.model.subject.User"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=admin$edit_user_done">
    <input type="hidden" name="id" value="${requestScope.user.id}"/>
    <table>
        <tr>
            <th><fmt:message key="table.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.user.name" bundle="${rb}"/></th>
            <th><fmt:message key="table.user.surname" bundle="${rb}"/></th>
            <th><fmt:message key="form.login.login" bundle="${rb}"/></th>
            <th><fmt:message key="form.login.password" bundle="${rb}"/></th>
            <th><fmt:message key="table.user.role" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <td>
            ${requestScope.user.id}
        </td>
        <td>
            <input type="text" name="name" value="${requestScope.user.name}"/>
        </td>
        <td>
            <input type="text" name="surname" value="${requestScope.user.surname}"/>
        </td>
        <td>
            <input type="text" name="login" value="${requestScope.user.login}"/>
        </td>
        <td>
            <input type="text" name="password" value="${requestScope.user.password}"/>
        </td>
        <td>
            <select name="role">
                <option value="driver"
                        <c:if test="${requestScope.user.role eq 'driver'}">
                            selected
                        </c:if>
                ><fmt:message key="role.driver" bundle="${rb}"/>
                </option>
                <option value="dispatcher"
                        <c:if test="${requestScope.user.role eq 'dispatcher'}">
                            selected
                        </c:if>
                ><fmt:message key="role.dispatcher" bundle="${rb}"/>
                </option>
                <option value="admin"
                        <c:if test="${requestScope.user.role eq 'admin'}">
                            selected
                        </c:if>
                ><fmt:message key="role.admin" bundle="${rb}"/>
                </option>
                <option value="guest"
                        <c:if test="${requestScope.user.role eq 'guest'}">
                            selected
                        </c:if>
                ><fmt:message key="role.guest" bundle="${rb}"/>
                </option>
            </select>
        </td>
        <td>
            <ct:button img="done" url="submit"/>
        </td>
        <td>
            <ct:button img="cancel" url="/controller?command=admin$show_table&selectTable=user"/>
        </td>
    </table>
</form>