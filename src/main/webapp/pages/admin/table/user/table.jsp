<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="user" class="by.epam.autobase.model.subject.User"/>


<table class="nth">
    <tr>
        <th><fmt:message key="table.id" bundle="${rb}"/></th>
        <th><fmt:message key="table.user.name" bundle="${rb}"/></th>
        <th><fmt:message key="table.user.surname" bundle="${rb}"/></th>
        <th><fmt:message key="form.login.login" bundle="${rb}"/></th>
        <th><fmt:message key="form.login.password" bundle="${rb}"/></th>
        <th><fmt:message key="table.user.role" bundle="${rb}"/></th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach items="${sessionScope.resultTable}" varStatus="user">
        <tr>
            <td> ${user.current.id} </td>
            <td> ${user.current.name} </td>
            <td> ${user.current.surname} </td>
            <td> ${user.current.login} </td>
            <td> ${user.current.password} </td>
            <td><fmt:message key="role.${user.current.role}" bundle="${rb}"/></td>
            <td>
                <ct:button img="edit" url="/controller?command=admin$edit_user_select&selectedid=${user.current.id}"/>
            </td>
            <td>
                <ct:button img="trash" url="/controller?command=admin$delete_user_done&selectedid=${user.current.id}"/>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<c:choose>
    <c:when test="${requestScope.tableCommandSelector eq 'editEntity'}">
        <c:import url="/pages/admin/table/user/edit.jsp"/>
    </c:when>
    <c:when test="${requestScope.tableCommandSelector eq 'createEntity'}">
        <c:import url="/pages/admin/table/user/create.jsp"/>
    </c:when>
    <c:otherwise>
        <ct:button img="add" url="/controller?command=admin$create_entity_select"/>
    </c:otherwise>
</c:choose>




