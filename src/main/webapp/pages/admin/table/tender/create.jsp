<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="tender" class="by.epam.autobase.model.subject.Tender"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=admin$create_tender_done">
    <input type="hidden" name="return" value="/controller?command=admin$show_table_tender">
    <table>
        <tr>
            <th><fmt:message key="table.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.driver_id" bundle="${rb}"/></th>
            <th><fmt:message key="table.tender.weight" bundle="${rb}"/></th>
            <th><fmt:message key="table.tender.city" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>+</td>
            <td>
                <input type="number" title="driver id" name="driver" value="0"/>
            </td>
            <td>
                <input type="number" title="weight, kg" name="weight" value="0"/>
            </td>
            <td>
                <input type="text" name="city"/>
            </td>
            <td>
                <ct:button img="done" url="submit"/>
            </td>
            <td>
                <ct:button img="cancel" url="/controller?command=admin$show_table&selectTable=tender"/>
            </td>
        </tr>
    </table>
</form>