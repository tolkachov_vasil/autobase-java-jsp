<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="tender" class="by.epam.autobase.model.subject.Tender"/>


<table class="nth">
    <tr>
        <th><fmt:message key="table.id" bundle="${rb}"/></th>
        <th><fmt:message key="table.driver_id" bundle="${rb}"/></th>
        <th><fmt:message key="table.tender.weight" bundle="${rb}"/></th>
        <th><fmt:message key="table.tender.city" bundle="${rb}"/></th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach items="${sessionScope.resultTable}" varStatus="tender">
        <tr>
            <td> ${tender.current.id} </td>
            <td> ${tender.current.driver} </td>
            <td> ${tender.current.weight} </td>
            <td> ${tender.current.city} </td>
            <td>
                <ct:button img="edit"
                           url="/controller?command=admin$edit_tender_select&selectedid=${tender.current.id}"/>
            </td>
            <td>
                <ct:button img="trash"
                           url="/controller?command=admin$delete_tender_done&selectedid=${tender.current.id}"/>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<c:choose>
    <c:when test="${requestScope.tableCommandSelector eq 'createEntity'}">
        <c:import url="/pages/admin/table/tender/create.jsp"/>
    </c:when>

    <c:when test="${requestScope.tableCommandSelector eq 'editEntity'}">
        <c:import url="/pages/admin/table/tender/edit.jsp"/>
    </c:when>

    <c:otherwise>
        <ct:button img="add" url="/controller?command=admin$create_entity_select"/>
    </c:otherwise>
</c:choose>




