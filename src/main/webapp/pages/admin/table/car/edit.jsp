<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="car" class="by.epam.autobase.model.subject.Car"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=admin$edit_car_done">
    <table>
        <tr>
            <th><fmt:message key="table.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.driver_id" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.brand" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.model" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.number" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.capacity" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.status" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <td>${requestScope.car.id}</td>
        <input type="hidden" name="id" value="${requestScope.car.id}"/>
        <td>
            <input type="number" name="driver" title="driver id" value="${requestScope.car.driver}"/>
        </td>
        <td>
            <input type="text" name="brand" value="${requestScope.car.brand}"/>
        </td>
        <td>
            <input type="text" name="model" value="${requestScope.car.model}"/>
        </td>
        <td>
            <input type="text" name="number" value="${requestScope.car.number}"/>
        </td>
        <!--</tr><tr>-->
        <td>
            <input type="number" name="capacity" title="capacity, kg" value="${requestScope.car.capacity}"/>
        </td>
        <td>
            <select name="status">
                <option value="valid"
                        <c:if test="${requestScope.car.status eq 'valid'}">
                            selected
                        </c:if>
                ><fmt:message key="car.status.valid" bundle="${rb}"/>
                </option>
                <option value="broken"
                        <c:if test="${requestScope.car.status eq 'broken'}">
                            selected
                        </c:if>
                ><fmt:message key="car.status.broken" bundle="${rb}"/>
                </option>
            </select>
        </td>
        <td>
            <ct:button img="done" url="submit"/>
        </td>
        <td>
            <ct:button img="cancel" url="/controller?command=admin$show_table&selectTable=car"/>
        </td>
    </table>
</form>