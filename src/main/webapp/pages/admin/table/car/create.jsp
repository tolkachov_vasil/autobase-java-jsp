<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="car" class="by.epam.autobase.model.subject.Car"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=admin$create_car_done">
    <input type="hidden" name="return" value="/controller?command=admin$show_table_car">
    <table>
        <tr>
            <th><fmt:message key="table.id" bundle="${rb}"/></th>
            <th><fmt:message key="table.driver_id" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.brand" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.model" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.number" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.capacity" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.status" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>+</td>
            <td>
                <input type="number" name="driver" title="driver id" value="0"/>
            </td>
            <td>
                <input type="text" name="brand"/>
            </td>
            <td>
                <input type="text" name="model"/>
            </td>
            <td>
                <input type="text" name="number"/>
            </td>
            <td>
                <input type="number" name="capacity" value="0"/>
            </td>
            <td>
                <select name="status">
                    <option value="valid" selected
                    ><fmt:message key="car.status.valid" bundle="${rb}"/>
                    </option>
                    <option value="broken"
                    ><fmt:message key="car.status.broken" bundle="${rb}"/>
                    </option>
                </select>
            </td>
            <td>
                <ct:button img="done" url="submit"/>
            </td>
            <td>
                <ct:button img="cancel" url="/controller?command=admin$show_table&selectTable=car"/>
            </td>
        </tr>
    </table>
</form>