<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="car" class="by.epam.autobase.model.subject.Car"/>


<table class="nth">
    <tr>
        <th><fmt:message key="table.id" bundle="${rb}"/></th>
        <th><fmt:message key="table.driver_id" bundle="${rb}"/></th>
        <th><fmt:message key="table.car.brand" bundle="${rb}"/></th>
        <th><fmt:message key="table.car.model" bundle="${rb}"/></th>
        <th><fmt:message key="table.car.number" bundle="${rb}"/></th>
        <th><fmt:message key="table.car.capacity" bundle="${rb}"/></th>
        <th><fmt:message key="table.car.status" bundle="${rb}"/></th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach items="${sessionScope.resultTable}" varStatus="car">
        <tr>
            <td> ${car.current.id} </td>
            <td> ${car.current.driver} </td>
            <td> ${car.current.brand} </td>
            <td> ${car.current.model} </td>
            <td> ${car.current.number} </td>
            <td> ${car.current.capacity} </td>
            <td><fmt:message key="car.status.${car.current.status}" bundle="${rb}"/></td>
            <td>
                <ct:button img="edit" url="/controller?command=admin$edit_car_select&selectedid=${car.current.id}"/>
            </td>
            <td>
                <ct:button img="trash" url="/controller?command=admin$delete_car_done&selectedid=${car.current.id}"/>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<c:choose>
    <c:when test="${requestScope.tableCommandSelector eq 'createEntity'}">
        <c:import url="/pages/admin/table/car/create.jsp"/>
    </c:when>

    <c:when test="${requestScope.tableCommandSelector eq 'editEntity'}">
        <c:import url="/pages/admin/table/car/edit.jsp"/>
    </c:when>

    <c:otherwise>
        <ct:button img="add" url="/controller?command=admin$create_entity_select"/>
    </c:otherwise>
</c:choose>




