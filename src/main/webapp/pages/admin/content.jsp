<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${sessionScope.selector eq 'showTable'}">
        <c:import url="/pages/admin/table/${sessionScope.selectTable}/table.jsp"/>
    </c:when>
</c:choose>

