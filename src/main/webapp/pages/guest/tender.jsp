<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="tender" class="by.epam.autobase.model.subject.Tender"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=create_tender_done">
    <input style="visibility: hidden" type="number" name="driver" value="0"/>
    <table>
        <tr>
            <th><fmt:message key="table.tender.city" bundle="${rb}"/></th>
            <th><fmt:message key="table.tender.weight" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>
                <input type="text" name="city"/>
            </td>
            <td>
                <input type="number" title="weight, kg" name="weight" value="0"/>
            </td>
            <td>
                <ct:button img="done" url="submit"/>
            </td>
            <td>
                <ct:button img="cancel" url="/index.jsp"/>
            </td>
        </tr>
    </table>
</form>