<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div>
    <form class="login-form align-right" name="loginForm" method="post" action="controller?command=login">
        <table>
            <tr>
                <td>
                    <fmt:message key="form.login.login" bundle="${rb}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="login" value=""/>
                    <%--<input type="text" name="login" value="admin"/>--%>
                    <%--<input type="text" name="login" value="ppp"/>--%>
                    <%--<input type="text" name="login" value="iii"/>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="form.login.password" bundle="${rb}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="password" name="password" value="111"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" style="width: 100%"
                           value='<fmt:message key="menu.guest.login" bundle="${rb}"/>'/>
                </td>
            </tr>
        </table>
    </form>
</div>
