<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<li><a href="/controller?command=driver$show_car">
    <fmt:message key="menu.driver.show_car" bundle="${rb}"/>
</a></li>

<li><a href="/controller?command=driver$show_trip">
    <fmt:message key="menu.driver.show_trip" bundle="${rb}"/>
</a></li>

