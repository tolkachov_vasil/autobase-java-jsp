<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:choose>
    <c:when test="${sessionScope.selector eq 'showCar'}">
        <c:import url="/pages/driver/table/car/table.jsp"/>
    </c:when>
    <c:when test="${sessionScope.selector eq 'showTrip'}">
        <c:import url="/pages/driver/table/trip/table.jsp"/>
    </c:when>
</c:choose>

