<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="car" class="by.epam.autobase.model.subject.Car"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=driver$edit_car_done">
    <table>
        <tr>
            <th><fmt:message key="table.car.brand" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.model" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.number" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.capacity" bundle="${rb}"/></th>
            <th><fmt:message key="table.car.status" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td> ${sessionScope.car.brand} </td>
            <td> ${sessionScope.car.model} </td>
            <td> ${sessionScope.car.number} </td>
            <td> ${sessionScope.car.capacity} </td>
            <td>
                <select name="status">
                    <option value="valid"
                            <c:if test="${sessionScope.car.status eq 'valid'}">
                                selected
                            </c:if>
                    ><fmt:message key="car.status.valid" bundle="${rb}"/>
                    </option>
                    <option value="broken"
                            <c:if test="${sessionScope.car.status eq 'broken'}">
                                selected
                            </c:if>
                    ><fmt:message key="car.status.broken" bundle="${rb}"/>
                    </option>
                </select>
            </td>
            <td>
                <ct:button img="done" url="submit"/>
            </td>
            <td>
                <ct:button img="cancel" url="/controller?command=driver$show_car"/>
            </td>
        </tr>
    </table>
</form>


