<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<jsp:useBean id="trip" class="by.epam.autobase.model.subject.Trip"/>
<jsp:useBean id="tender" class="by.epam.autobase.model.subject.Tender"/>

<c:choose>
    <c:when test="${not empty sessionScope.tender.city}">
        <form class="input-form" name="entityForm" method="post"
              action="/controller?command=driver$edit_trip_done">
            <table>
                <tr>
                    <th><fmt:message key="table.tender.city" bundle="${rb}"/></th>
                    <th><fmt:message key="table.tender.weight" bundle="${rb}"/></th>
                    <th><fmt:message key="table.trip.status" bundle="${rb}"/></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td> ${sessionScope.tender.city} </td>
                    <td> ${sessionScope.tender.weight} </td>
                    <td>
                        <select name="status">
                            <option value="idle"
                                    <c:if test="${sessionScope.trip.status eq 'idle'}">
                                        selected
                                    </c:if>
                            ><fmt:message key="trip.status.idle" bundle="${rb}"/>
                            </option>
                            <option value="accepted"
                                    <c:if test="${sessionScope.trip.status eq 'accepted'}">
                                        selected
                                    </c:if>
                            ><fmt:message key="trip.status.accepted" bundle="${rb}"/>
                            </option>
                            <option value="ended"
                                    <c:if test="${sessionScope.trip.status eq 'ended'}">
                                        selected
                                    </c:if>
                            ><fmt:message key="trip.status.ended" bundle="${rb}"/>
                            </option>
                        </select>
                    </td>
                    <td>
                        <ct:button img="done" url="submit"/>
                    </td>
                    <td>
                        <ct:button img="cancel" url="/controller?command=driver$show_trip"/>
                    </td>
                </tr>
            </table>
        </form>
    </c:when>
    <c:otherwise>
        <div class="message"
             onclick="this.setAttribute('hidden', 'true')">
            Trip is not available
        </div>
    </c:otherwise>
</c:choose>

