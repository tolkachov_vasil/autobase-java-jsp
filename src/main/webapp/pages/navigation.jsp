<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<ul class="navbar">
    <li class="navbar-banner">
        <a class="disabled" href="#">&nbsp;<fmt:message key="title.banner" bundle="${rb}"/></a>
    </li>

    <li class="separator">
        <a class="disabled" href="#"></a>
    </li>


    <li class="navbar-menu">
        <jsp:include page="/pages/${sessionScope.role}/menu.jsp"/>
    </li>

    <ul class="button-black align-right">

        <li>
            <a class="disabled" href="#">
                <fmt:message key="role.${sessionScope.role}" bundle="${rb}"/>
            </a>
        </li>

        <li>
            <a class="img" href="/controller?command=set_En_Locale">
                <img src="/img/united_kingdom_flag.png" alt="en">
            </a>
        </li>
        <li>
            <a class="img" href="/controller?command=set_Ru_Locale">
                <img src="/img/russia_flag.png" alt="ru">
            </a>
        </li>

        <li class="navbar-auth">
            <c:choose>
                <c:when test="${sessionScope.role eq 'guest'}">
                    <a href="/controller?command=show_Login_Form">
                        <fmt:message key="menu.guest.login" bundle="${rb}"/>
                    </a>
                </c:when>
                <c:otherwise>
                    <a href="/controller?command=logout">
                        <fmt:message key="menu.user.logout" bundle="${rb}"/>
                    </a>
                </c:otherwise>
            </c:choose>
        </li>
    </ul>
</ul>