<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="user" class="by.epam.autobase.model.subject.User"/>
<jsp:useBean id="car" class="by.epam.autobase.model.subject.Car"/>

<table class="nth">
    <tr>
        <th><fmt:message key="table.user.name" bundle="${rb}"/></th>
        <th><fmt:message key="table.user.surname" bundle="${rb}"/></th>
        <th><fmt:message key="menu.driver.show_car" bundle="${rb}"/></th>
        <th></th>
        <th></th>
        <th><fmt:message key="table.car.capacity" bundle="${rb}"/></th>
        <th><fmt:message key="table.car.status" bundle="${rb}"/></th>
    </tr>
    <c:forEach items="${sessionScope.allDrivers}" varStatus="user">
        <tr>
            <td> ${user.current.name} </td>
            <td> ${user.current.surname} </td>
            <c:forEach items="${sessionScope.allCars}" varStatus="car">
                <c:if test="${car.current.id eq user.current.id}">
                    <td> ${car.current.brand} </td>
                    <td> ${car.current.model} </td>
                    <td> ${car.current.number} </td>
                    <td> ${car.current.capacity} </td>
                    <td><fmt:message key="car.status.${car.current.status}" bundle="${rb}"/></td>
                </c:if>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
<br>





