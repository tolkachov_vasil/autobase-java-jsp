<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="trip" class="by.epam.autobase.model.subject.Trip"/>
<jsp:useBean id="car" class="by.epam.autobase.model.subject.Car"/>
<jsp:useBean id="tender" class="by.epam.autobase.model.subject.Tender"/>


<table class="nth">
    <tr>
        <th><fmt:message key="menu.driver.show_car" bundle="${rb}"/></th>
        <th></th>
        <th></th>
        <th><fmt:message key="table.tender.city" bundle="${rb}"/></th>
        <th><fmt:message key="table.tender.weight" bundle="${rb}"/></th>
        <th><fmt:message key="table.trip.status" bundle="${rb}"/></th>
    </tr>
    <c:forEach items="${sessionScope.allTrips}" varStatus="trip">
        <tr>
            <c:forEach items="${sessionScope.allCars}" varStatus="car">
                <c:if test="${trip.current.car eq car.current.id}">
                    <td> ${car.current.brand} </td>
                    <td> ${car.current.model} </td>
                    <td> ${car.current.number} </td>
                </c:if>
            </c:forEach>
            <c:forEach items="${sessionScope.allTenders}" varStatus="tender">
                <c:if test="${trip.current.tender eq tender.current.id}">
                    <td> ${tender.current.city} </td>
                    <td> ${tender.current.weight} </td>
                </c:if>
            </c:forEach>
            <td><fmt:message key="trip.status.${trip.current.status}" bundle="${rb}"/></td>
        </tr>
    </c:forEach>
</table>




