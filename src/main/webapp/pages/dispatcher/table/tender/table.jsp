<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="tender" class="by.epam.autobase.model.subject.Tender"/>
<jsp:useBean id="user" class="by.epam.autobase.model.subject.User"/>

<table class="nth">
    <tr>
        <th><fmt:message key="table.tender.city" bundle="${rb}"/></th>
        <th><fmt:message key="table.tender.weight" bundle="${rb}"/></th>
        <th><fmt:message key="table.user.name" bundle="${rb}"/></th>
        <th><fmt:message key="table.user.surname" bundle="${rb}"/></th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach items="${sessionScope.allTenders}" varStatus="tender">
        <tr>
            <td> ${tender.current.city} </td>
            <td> ${tender.current.weight} </td>
            <c:choose>
                <c:when test="${tender.current.driver > 0}">
                    <c:forEach items="${sessionScope.allDrivers}" varStatus="user">
                        <c:if test="${tender.current.driver eq user.current.id}">
                            <td> ${user.current.name} </td>
                            <td> ${user.current.surname} </td>
                        </c:if>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <td></td>
                    <td></td>
                </c:otherwise>
            </c:choose>
            <td>
                <a href="/controller?command=dispatcher$edit_tender_select&selectedid=${tender.current.id}">
                    <img class="button-img" src="/img/edit.png">
                </a>
            </td>
            <td>
                <a href="/controller?command=dispatcher$delete_tender_done&selectedid=${tender.current.id}">
                    <img class="button-img" src="/img/trash.png">
                </a>
            </td>
        </tr>
    </c:forEach>
</table>
<br>

<c:choose>
    <c:when test="${requestScope.tableCommandSelector eq 'editEntity'}">
        <c:import url="/pages/dispatcher/table/tender/edit.jsp"/>
    </c:when>
</c:choose>




