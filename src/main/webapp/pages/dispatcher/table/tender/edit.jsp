<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ct" uri="http://mycompany.com" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="tender" class="by.epam.autobase.model.subject.Tender"/>
<jsp:useBean id="user" class="by.epam.autobase.model.subject.User"/>

<form class="input-form" name="entityForm" method="post"
      action="/controller?command=dispatcher$edit_tender_done">
    <input type="hidden" name="return" value="/controller?command=dispatcher$show_tenders">
    <input type="hidden" name="tenderId" value="${requestScope.tender.id}"/>
    <table>
        <tr>
            <th><fmt:message key="role.driver" bundle="${rb}"/></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>
                <select name="driver">
                    <c:forEach items="${requestScope.validDrivers}" varStatus="user">
                        <option value="${user.current.id}"
                                <c:if test="${requestScope.tender.driver eq user.current.id}">
                                    selected
                                </c:if>
                        >
                                ${user.current.name} ${user.current.surname}
                        </option>
                    </c:forEach>
                    <option value="0">
                        -
                    </option>
                </select>
            </td>
            <td>
                <ct:button img="done" url="submit"/>
            </td>
            <td>
                <ct:button img="cancel" url="/controller?command=dispatcher$show_tenders"/>
            </td>
        </tr>
    </table>
</form>
