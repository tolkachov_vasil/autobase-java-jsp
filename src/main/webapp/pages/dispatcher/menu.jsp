<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<li><a href="/controller?command=dispatcher$show_tenders">
    <fmt:message key="menu.admin.show_tenders" bundle="${rb}"/>
</a></li>

<li><a href="/controller?command=dispatcher$show_drivers">
    <fmt:message key="menu.dispatcher.show_drivers" bundle="${rb}"/>
</a></li>

<li><a href="/controller?command=dispatcher$show_trips">
    <fmt:message key="menu.admin.show_trips" bundle="${rb}"/>
</a></li>

