<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="user" class="by.epam.autobase.model.subject.User"/>

<c:if test="${empty sessionScope.role}">
    <c:set var="role" value="guest" scope="session"/>
</c:if>


<c:if test="${empty sessionScope.locale}">
    <c:set var="locale" value="en" scope="session"/>
</c:if>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb" scope="session"/>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="/css/style.css">
    <title>
        <fmt:message key="title.banner" bundle="${rb}"/>
    </title>
</head>
<body>

<wrapper>

    <c:import url="/pages/navigation.jsp"/>

    <c:if test="${not empty requestScope.message}">
        <div class="message"
             onclick="this.setAttribute('hidden', 'true')">
                ${requestScope.message}
        </div>
    </c:if>

    <c:if test="${sessionScope.selector eq 'showLoginForm'}">
        <div>
            <c:import url="/pages/login.jsp"/>
        </div>
    </c:if>

    <content>
        <c:import url="/pages/${sessionScope.role}/content.jsp"/>
    </content>

    <footer>
        <fmt:message key="title.footer" bundle="${rb}"/>
    </footer>

</wrapper>
</body>
</html>