import by.epam.autobase.resource.Constant;
import by.epam.autobase.resource.ContentManager;
import junit.framework.TestCase;
import org.junit.Test;

public class ContentManagerTest extends TestCase {

    @Test
    public void testGetProperty() {

        assertEquals(ContentManager.getProperty(
                Constant.LOCALE_EN, "menu.admin.show_users"), "Users");

        assertEquals(ContentManager.getProperty(
                Constant.LOCALE_RU, "menu.admin.show_users"), "Пользователи");

    }
}
